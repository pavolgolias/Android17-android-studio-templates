Android tutorial 17

Specific Android Studio Templates testing
-----------------------------------------

* Fullscreen Activity - full screen activity with content over the whole display
* Google AdMod Ads Activity - example of typical ads activity, which are often shown in free apps with paid features
* Google Maps Activity
* Login Activity - example how to implement login activity
* Master Detail Flow - very nice example of master-detail activity with some dummy data
* Navigation Drawer Activity - example of left side menu
* Scrolling Activity - scrolling activity used in material design
* settings activity - example of various settings with groups
* Tabbed activity
